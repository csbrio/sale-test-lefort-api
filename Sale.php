<?php
require 'settings/Conexion.php';
class Sale extends Conexion {

    public function __construct() {
        parent::__construct();
    }
    
    public function __destruct() {
        if (isset($stmt))
            $stmt->closeCursor();
    }

    public function get_sales()
    {
        $sql = 'SELECT 
                *
                FROM
                sale JOIN customer ON sale.customer_id = customer.id';


        $stmt = $this->conexion_db->prepare($sql);
        $stmt->execute();
        
        $sales = array();
        while ($sale = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $sales[] = $sale;
        }

        echo json_encode(array('status'=> TRUE, 'sales' => $sales));
    }
}

$data = json_decode(file_get_contents("php://input"), true);
var_dump($data);
if (isset($data['method'])){
    $objSale = new User();
    if ($data['method'] === 'get_sales') {
        $objSale->get_sales();
    }
}
