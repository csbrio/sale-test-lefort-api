CREATE TABLE customer( id INT AUTO_INCREMENT, fullname VARCHAR(255), email VARCHAR(50), PRIMARY KEY(id));CREATE TABLE sale( id INT AUTO_INCREMENT , customer_id INT NOT NULL, sale_date DATETIME NOT NULL, total DECIMAL(17,2) NOT NULL, PRIMARY KEY(id), FOREIGN KEY(customer_id) REFERENCES customer(id));CREATE TABLE sold_item( id INT AUTO_INCREMENT, sale_id INT NOT NULL, description VARCHAR(255), department VARCHAR(30), price DECIMAL(17,2) NOT NULL, PRIMARY KEY(id), FOREIGN KEY(sale_id) REFERENCES sale(id), CONSTRAINT chk_deparment CHECK(department IN('sports','home','beauty','food','pets')));

INSERT INTO sale_test.customer (fullname,email)
	VALUES ('Carlos Briones','briones.cs@gmail.com');
INSERT INTO sale_test.customer (fullname,email)
	VALUES ('Foo Bar','foobar@test.com');

INSERT INTO sale_test.sale (customer_id,sale_date,total)
	VALUES (1,'2021-12-13 03:20:00',2700.00);
INSERT INTO sale_test.sale (customer_id,sale_date,total)
	VALUES (2,'2021-12-13 04:20:00',7200.00);
